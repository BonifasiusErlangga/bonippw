from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Matkul

# Create your views here.
def masuk(request):
    matkul = Matkul.objects.all()
    html = 'story5/story5.html'
    context = {'matkul':matkul}
    return render(request, html, context)

def addMatkul(request):
    c = request.POST['nama']
    new_item = Matkul(nama = request.POST['nama'])
    new_item.save()
    return HttpResponseRedirect('/story5/')

def deleteMatkul(request, context_id):
    item_to_delete = Matkul.objects.get(id=context_id)
    item_to_delete.delete()
    return HttpResponseRedirect('/story5/')



