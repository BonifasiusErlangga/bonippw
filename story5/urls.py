from django.urls import path

from . import views

app_name = 'story5'

urlpatterns = [
    path('story5',views.masuk, name='story5'),
    path('story5',views.addMatkul, name='story5'),
]
