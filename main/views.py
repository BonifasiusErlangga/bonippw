from django.shortcuts import render


def home(request):
    return render(request, 'main/home.html')

def profil(request):
    return render(request, 'main/profil.html')

def school(request):
    return render(request, 'main/school.html')

def about(request):
    return render(request, 'main/about.html')

def story1(request):
    return render(request, 'main/story1.html')

def story5(request):
    return render(request, 'story5/story5.html')








