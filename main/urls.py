from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('profil.html',views.profil, name='profil'),
    path('school.html',views.school, name='school'),
    path('about.html',views.about, name='about'),
    path('home.html',views.home, name='home'),
    path('story1.html',views.story1, name='story1'),
    path('story5.html',views.story5, name='story5'),
]

